package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true) {
            System.out.printf("Let's play round %s", roundCounter); //starting game
            roundCounter++;

            int rand = (int) (3 * Math.random()); //makes random computer choice
            String computerChoice; //empty string
            if (rand == 0) {
                computerChoice = "rock"; //computer picks rock
            } else if (rand == 1) {
                computerChoice = "paper"; //computer picks paper
            } else {
                computerChoice = "scissors"; //computer picks scissors
            }

            String humanChoice;
            while (true) {
                humanChoice = readInput("\nYour choice (Rock/Paper/Scissors)?").toLowerCase();

                if (rpsChoices.contains(humanChoice)) {
                    break;
                }
                System.out.printf("I do not understand %s. Could you try again?", humanChoice);
            }

            if (humanChoice.equals(computerChoice)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!", humanChoice, computerChoice);
            } else if (humanChoice.equals("rock") && computerChoice.equals("scissors")) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!", humanChoice, computerChoice);
                humanScore++;
            } else if (humanChoice.equals("paper") && computerChoice.equals("rock")) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!", humanChoice, computerChoice);
                humanScore++;
            } else if (humanChoice.equals("scissors") && computerChoice.equals("paper")) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!", humanChoice, computerChoice);
                humanScore++;
            } else {
                System.out.printf("Human chose %s, computer chose %s. Computer wins!", humanChoice, computerChoice);
                computerScore++;
            }

            System.out.printf("\nScore: human %s, computer %s", humanScore, computerScore);

            String continueGame = readInput("\nDo you wish to continue playing? (y/n)?");
            if (continueGame.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        }
    }


    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
